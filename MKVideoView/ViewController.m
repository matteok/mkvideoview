//
//  ViewController.m
//  MKVideoView
//
//  Created by Rolf Koczorek on 15.07.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

#import "ViewController.h"
#import "MKVideoView.h"

@interface ViewController ()

@property (nonatomic, strong) MKVideoView *videoView;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.videoView = [[MKVideoView alloc] initWithFrame:CGRectMake(100, 100, 500, 350)];
    [self.view addSubview:self.videoView];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"video" ofType:@"mp4"];
    self.videoView.videoURL = [NSURL fileURLWithPath:path];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
