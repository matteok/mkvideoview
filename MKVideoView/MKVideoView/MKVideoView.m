//
//  MKVideoView.m
//  MKVideoView
//
//  Created by Rolf Koczorek on 15.07.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//

#import "MKVideoView.h"

@interface MKVideoView()

@property (nonatomic, strong) UIImageView *thumbnailImageView;
@property (nonatomic) CGRect innerFrame;
@property (nonatomic, strong) UIButton *playButton;

@end

@implementation MKVideoView

@synthesize videoURL = _videoURL;
@synthesize thumbnailImage = _thumbnailImage;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self construct];
    }
    return self;
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self) {
        [self construct];
    }
    return self;
}

-(void) construct
{
    
    self.moviePlayerController = [[MPMoviePlayerController alloc] init];
    self.moviePlayerController.view.frame = self.innerFrame;
    [self addSubview:self.moviePlayerController.view];
    
    self.thumbnailImageView = [[UIImageView alloc] initWithFrame:self.innerFrame];
    [self addSubview:self.thumbnailImageView];
    
    CGFloat buttonSize = 74;
    self.playButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - buttonSize/2, self.frame.size.height/2 - buttonSize/2, buttonSize, buttonSize)];
    [self.playButton setBackgroundImage:[UIImage imageNamed:@"PlayButton.png"] forState:UIControlStateNormal];
    [self.playButton setBackgroundImage:[UIImage imageNamed:@"PlayButtonPushed.png"] forState:UIControlStateHighlighted];
    [self addSubview:self.playButton];
    
    [self.playButton addTarget:self action:@selector(play) forControlEvents:UIControlEventTouchUpInside];
}


#pragma mark - player controls

-(void) play
{
    [self.moviePlayerController setFullscreen:YES animated:YES];
    [self.moviePlayerController play];
}

-(void) stop
{
    [self.moviePlayerController setFullscreen:NO animated:YES];
    [self.moviePlayerController stop];
}

#pragma mark - setters

-(void) setFrame:(CGRect)frame
{
    [super setFrame:frame];
    self.moviePlayerController.view.frame = self.innerFrame;
    self.thumbnailImageView.frame = self.innerFrame;
}

-(void) setVideoURL:(NSURL *)videoURL
{
    _videoURL = videoURL;
    NSLog(@"set url: %@", videoURL);
    self.moviePlayerController.contentURL = videoURL;
}

-(void) setThumbnailImage:(UIImage *)thumbnailImage
{
    _thumbnailImage = thumbnailImage;
    self.thumbnailImageView.image = thumbnailImage;
}

#pragma mark - getters

-(CGRect) innerFrame
{
    return CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
