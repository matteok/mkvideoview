//
//  MKVideoView.h
//  MKVideoView
//
//  Created by Rolf Koczorek on 15.07.14.
//  Copyright (c) 2014 com.matteo-koczorek. All rights reserved.
//
#include <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>

@interface MKVideoView : UIView

@property (nonatomic, strong) MPMoviePlayerController *moviePlayerController;
@property (nonatomic, strong) NSURL *videoURL;
@property (nonatomic, strong) UIImage *thumbnailImage;

-(void) play;
-(void) stop;

@end
